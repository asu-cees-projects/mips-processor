package main_project;

public class ALU {
    private static boolean zeroFLAG;// is true when inputs are equal and false otherwise
    private static int input0,input1;//values to be received from register memory
    private static int control;//value to be received from ALU Control
    private static int output;// value to be sent out to the mux
    private static int sht;
   /* public ALU()
    {
        //input0=0;
        //input1=0;
    }*/
    public static void compute()
    {
        input0=RF.getReadD1(); // to get value from Read data 1 output of the register memory
        input1=muxalu.output;  // to get value of output of the mux right before the ALU
        sht=insMem.getInsOutput().getShamt();
        control=ALUcontrol.getAlucontrol();
        zeroFLAG=false;
        switch (control)
        {
            case 0:// AND operation
                output= (input0&input1); // bitwise and operation
                break;
            case 1:// OR operation
                output=(input0|input1); // bitwise or operation
                break;
            case 2: //add instruction
                output=input0+input1;// to be used for and, andi and lw/sw where the base address in input0 is added to the offset in input1
                break;
            case 3://nor instruction
                output=(-1*~(input0 | input1));//bitwise nor
                break;
            case 6: //subtract operation for beq instruction
                output=input0-input1;
                break;
            case 7://slt instruction/ slti instructions
                if(input0<input1)
                {
                    output=1;
                }
                else
                    output=0;
                break;
            case 8: //sll instruction
                output=input1<<sht; //shift input0 by number of bits 
                break;
        }
        if(output==0)
        {
            zeroFLAG=true;// trigger branch operation
        }
        System.out.println("ALU output is"+output);
    }
    public static int getOutput()
    {
        return output;
    }
    public static boolean getZeroFLAG()
    {
        return zeroFLAG;
    }
}
