package main_project;
public class ControlUnit
{
    //fields
    //Control Unit Input
    private static int opcode;            //refers to the operation code of instruction

    //Control Unit outputs
    private static boolean RegDst ;       //(signal to a mux)if true selects [15:11] bits of instruction (r-type)
                                   // else selects [20:16] bits (i-type) and sends it signal to thr multiplexor
    private static boolean RegWrite;      // if true The register on the Write register input is written with the value on the Write data input.
                                  // else nothing is done
    private static boolean Alusrc;       // (signal sent to a mux)if true The second ALU operand is the signextended , lower 16 bits of the instruction.
                                  // else The second ALU operand comes from the second register file output (Read data 2).
    private static boolean Branch;       //(to be anded with the zero flag) if true then its a branch instruction and pc will be replaced by the branch address
                                  // else normal pc increment
    private static int MemRead;      //if true Data memory contents designated by the address input are put on the Read data output.
                                  // else nothing is done
    private static int MemWrite;     //if true Data memory contents designated by the address input are replaced by the value on the Write data input.
                                  // else nothing is done
    private static boolean MemtoReg;     //(signal to a mux)if true then The value fed to the register Write data input comes from the data memory.
                                  //else The value fed to the register Write data input comes from the ALU.
    private static boolean jalop;        //(signal sent to additional mux that we've created) true in case of jump and link instruction
    private static boolean jump ;        //(signal sent to mux) true in case of jump instructions
    private static boolean jr;           //(signal sent to mux) true in case of jump register instructions
    private static boolean link;         //(signal sent to mux)if true link ra 
    private static int ALUOPs;           // alu operation code to be sent to alu control unit



    //constructor
    public static void use (int op)       //opcode is specified at construction time then accordingly the outputs are set
    {
        opcode=op;
        switch (opcode)
        {
            //jr flag to be added
            //in case of jr instruction the flag is true and do the correct signal
            //r-type instructions have the same output
            case 0:              //control signals available in textbook (copies as as )
                RegDst=true;
                Alusrc=false;
                MemtoReg=false;
                RegWrite=true;
                MemRead=0;
                MemWrite=0;
                Branch=false;
                jalop=false;
                jump=false ;
                jr=false;
                ALUOPs=0b10;
                System.out.println("R");
                break;
            //i-type instructions
            case 0b100011:           //lw
                RegDst=false;
                Alusrc=true;
                MemtoReg=true;
                RegWrite=true;
                MemRead=1;
                MemWrite=0;
                Branch=false;
                jalop=false;
                jump=false ;
                jr=false;
                link=false ;
                ALUOPs=0b00;
                System.out.println("lw");
                break;
            case 0b101011 :         //sw
                RegDst=false;
                Alusrc=true;
                MemtoReg=true;
                RegWrite=false;
                MemRead=0;
                MemWrite=1;
                Branch=false;
                jalop=false;
                jump=false ;
                jr=false;
                link=false ;
                ALUOPs=0b00;
                System.out.println("sw");
                break;
            case 0b000100:         //beq
                RegDst=false;
                Alusrc=false;
                MemtoReg=true;
                RegWrite=false;
                MemRead=0;
                MemWrite=0;
                Branch=true;
                jalop=false;
                jump=false ;
                ALUOPs=0b01;
                link=false ;
                jr=false;
                System.out.println("beq");
                break;
            case 0b001000:                //addi
                RegDst=false;        // rt field (i-type)
                Alusrc=true;         // second operand of alu is a sign extend value (constant field (i-type))
                MemtoReg=false;      // data to be written in register coming from alu
                RegWrite=true;       // result must be stored in a reg.
                MemRead=0;            // no need to read data form memory
                MemWrite=0;          // no need to write data in memory
                Branch=false;        // it is not a branch inst.
                jalop=false;         // not a jal instruction
                jump=false ;         // not a jump instruction
                jr=false;
                link=false ;
                ALUOPs=0b00;         // addition operation is required from alu
                System.out.println("addi");
                break;
            case 0b100000:                //lb
                RegDst=false;        // rt field (i-type)
                Alusrc=true;         // second operand of alu is a sign extend value (constant field (i-type))
                MemtoReg=true;       // data to be written in register is from memory
                RegWrite=true;       // data from memory is stored in a register
                MemRead=2;        // true to read data from memory
                MemWrite=0;      // we will not store values in memory
                Branch=false;        // not a branch instruction
                jalop=false;         // not a jal instruction
                jump=false ;         // not a jump instruction
                jr=false;
                link=false ;
                ALUOPs=0b00;         // addition operation is required from alu
                System.out.println("lb");
                break;
            case 0b100100:                //lbu
                RegDst=false;        // rt field (i-type)
                Alusrc=true;         // second operand of alu is a sign extend value (constant field (i-type))
                MemtoReg=true;       // data to be written in register is from memory
                RegWrite=true;       // data from memory is stored in a register
                MemRead=3;        // true to read data from memory
                MemWrite=0;      // we will not store values in memory
                Branch=false;        // not a branch instruction
                jalop=false;         // not a jal instruction
                jump=false ;         // not a jump instruction
                jr=false;
                link=false ;
                ALUOPs=0b00;         // addition operation is required from alu (offset to base address in memory)
                System.out.println("lbu");
                break;
            case 0b101000:                //sb
                RegDst=false;        // rt field (i-type)
                Alusrc=true;         // second operand of alu is a sign extend value (constant field (i-type))
                MemtoReg=true;       // dont care (can be removed)
                RegWrite=false;      // no value to be stored in a register
                MemRead=0;           // we dont need to read from memory
                MemWrite=2;       // to store value from register
                Branch=false;        // not a branch instruction
                jalop=false;         // not a jal instruction
                jump=false ;         // not a jump instruction
                jr=false;
                link=false ;
                ALUOPs=0b00;         // addition operation is required from alu (offset to base address in memory)
                System.out.println("sb");
                break;
            case 0b001010:                //slti
                RegDst=false;        // rt field (i-type)
                Alusrc=true;         // second operand of alu is a sign extend value (constant field (i-type))
                MemtoReg=false;      // data to be written in register is from alu
                RegWrite=true;       // to store 1 if true or 0 if false
                MemRead=0;       // no use of memory
                MemWrite=0;      // no use of memory
                Branch=false;        // not a branch instruction
                jalop=false;         // not a jal instruction
                jump=false ;         // not a jump instruction
                jr=false;
                link=false ;
                ALUOPs=0b11;         // subtraction operation is required from alu (offset - register -ve if reg>offset ,+ve reg<offset)
                System.out.println("slti");
                break;
            //J-type instructions
            case 0b000010:                // jump
                jump=true ;          // all other control unit signals are not used therefore we dont care about setting values to them
                Branch = false;
                jr=false;
                System.out.println("j");
                break;
            case 0b000011:                //jump and link
                jalop=true;          // all other control unit signals are not used therefore we dont care about setting values to them
                RegWrite=true;       // to write the value of $ra
                link=true ;
                jump = true;
                Branch=false;
                jr=false;
                System.out.println("jal");
                break;

        }

    }

    //getters
    public static boolean isRegDst()
    {
        return RegDst;
    }

    public static boolean isRegWrite()
    {
        return RegWrite;
    }
    public static boolean isLink()
    {
        return link;
    }

    public static boolean isAlusrc()
    {
        return Alusrc;
    }

    public static boolean isBranch()
    {
        return Branch;
    }

    public static int getMemRead()
    {
        return MemRead;
    }

    public static int getMemWrite()
    {
        return MemWrite;
    }

    public static boolean isMemtoReg()
    {
        return MemtoReg;
    }

    public static int getALUOPs()
    {
        return ALUOPs;
    }

    public static boolean isJalop()
    {
        return jalop;
    }

    public static boolean isJump()
    {
        return jump;
    }

    public static boolean isJr() {
        return jr;
    }
}
