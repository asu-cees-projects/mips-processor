package main_project;
 class Output 
{
     public static String wireOut="";
     public static String Output()
    {
        wireOut="";
        String inst=intToBinary(insMem.insOutput.getOpcode(), 6);
        
        if(insMem.insOutput instanceof R_type)
           {
               inst+=intToBinary(insMem.insOutput.getRs(), 5);
               inst+=intToBinary(insMem.insOutput.getRt(), 5);               
               inst+=intToBinary(insMem.insOutput.getRd(), 5);
               inst+=intToBinary(insMem.insOutput.getShamt(), 5);
               inst+=intToBinary(insMem.insOutput.getFunct(), 6);
               wireOut+="Instruction : "+inst+"\n";
               wireOut+="Intruction [25-0] to Shift left 2: " + inst.substring(6, 32)+"\n";
               wireOut+="Instruction Memory to Control (Opcode): " + insMem.insOutput.getOpcode()+"\n";
               wireOut+="Intruction Memory to Read register 1 (rs): " + insMem.insOutput.getRs()+"\n";
               wireOut+="Intruction Memory to Read register 2 & RegDst Mux (rt): " + insMem.insOutput.getRt()+"\n";
               wireOut+="Intruction Memory to RegDst Mux (rd): " + insMem.insOutput.getRd()+"\n";
               wireOut+="Shift Amount: " + insMem.insOutput.getShamt() + "\n";
               wireOut+="Function Code: " + insMem.insOutput.getFunct() + "\n";
               wireOut+="Intruction [15-0] to sign extend(as 16 bits) and from sign extend to ALUSrc Mux and Shift left (as 32 bits): " + Integer.parseInt(inst.substring(16, 32),2)+"\n";
             }

        if(insMem.insOutput instanceof I_type)
        {
            inst+=intToBinary(insMem.insOutput.getRs(), 5);
            inst+=intToBinary(insMem.insOutput.getRt(), 5);
            inst+=intToBinary(insMem.insOutput.getConstant(), 16);
            wireOut+="Instruction: " + inst + "\n";
            wireOut+="Intruction [25-0] to Shift left 2: " + inst.substring(6, 32)+"\n";
            wireOut+="Instruction Memory to Control (Opcode): " + insMem.insOutput.getOpcode() + "\n";
            wireOut+="Instruction Memory to Read register 1 (rs): " + insMem.insOutput.getRs() + "\n";
            wireOut+="Instruction Memory to Read register 2 & RegDst Mux (rt): " + insMem.insOutput.getRt() + "\n";
            wireOut+="Instruction Memory to RegDst Mux (rd): " + inst.substring(16, 21) + "\n";
            wireOut+="Instruction [15-0] to sign extend(as 16 bits) and from sign extend to ALUSrc Mux and Shift left (as 32 bits): " + Integer.parseInt(inst.substring(16,32),2) + "\n";
            wireOut+="Instruction [10-6]: " + inst.substring(21, 26) + "\n";
            wireOut+="Instruction [5-0]: " + inst.substring(26, 32) + "\n";
        }
        if(insMem.insOutput instanceof J_type)
        {
            inst+=intToBinary(insMem.insOutput.getConstant(), 26);
            wireOut+="Instruction: " + inst + "\n";
            wireOut+="Intruction [25-0] to Shift left 2: " + inst.substring(6, 32)+"\n";
            wireOut+="Instruction Memory to Control (Opcode): " + inst.substring(0, 6) + "\n";
            wireOut+="Instruction Memory to Read register 1 (rs): " + inst.substring(6,11) + "\n";
            wireOut+="Instruction Memory to Read register 2 & RegDst Mux (rt): " + inst.substring(11,16) + "\n";
            wireOut+="Instruction Memory to RegDst Mux (rd): " + inst.substring(16, 21) + "\n";
            wireOut+="Instruction [15-0] to sign extend(as 16 bits) and from sign extend to ALUSrc Mux and Shift left (as 32 bits): " + Integer.parseInt(inst.substring(16,32),2) + "\n";
             wireOut+="Instruction [10-6]: " + inst.substring(21, 26) + "\n";
            wireOut+="Instruction [5-0] :" + inst.substring(26, 32) + "\n";
        }
        //int x=(Integer.parseInt(inst.substring(6, 32),2)*4);
        //int y=x*4;
        //wireOut+="PC Address:" + insMem.currentPC + "\n";
        wireOut+="RegDst Mux to Link Mux: "+ muxwr.output+ "\n";
        wireOut+="Link Mux to Write Register: " + muxlink.output + "\n";
        wireOut+="Read Data 1 to ALU input: " + RF.getReadD1() + "\n";
        wireOut+="Read Data 2 to Write Data in Data Memory and ALUsrc Mux: " + RF.getReadD2() + "\n";
        wireOut+="ALU second input and ALUsrc Mux output: " + muxalu.output + "\n";
        wireOut+="ALU output, Address input and MemtoReg Mux input: " + ALU.getOutput() + "\n";
        wireOut+="Zero Flag: " + ALU.getZeroFLAG() + "\n";
        wireOut+="Data Memory Output and MemtoReg Mux input: " + dataMem.readData + "\n";
        wireOut+="MemtoReg Mux output and JALOP Mux input: " + muxMem.output + "\n";
        wireOut+="Left Adder Output: "+ (insMem.getCurrentPC()+4) + "\n";
        wireOut+="JALOP Mux input: " + (insMem.currentPC + 4) + "\n";
        wireOut+="JALOP Mux output and Write Data in Register File: " + muxjalop.output + "\n";
        wireOut+="Upper Adder input: "+ (insMem.getCurrentPC()+4) + " and " + (Integer.parseInt(inst.substring(16, 32),2)*4) + "\n";
        wireOut+="Upper Adder output: "+ ((insMem.getCurrentPC()+4) + (Integer.parseInt(inst.substring(16, 32),2)*4)) + "\n";
        wireOut+="Branch Mux input: "+ ((insMem.getCurrentPC()+4) + (Integer.parseInt(inst.substring(16, 32),2)*4)) + " and " + (insMem.getCurrentPC()+4) + "\n";
        wireOut+="Branch Mux output: " + muxbranch.output + "\n";
        wireOut+="Jump Mux Input: " + "0000" +intToBinary((Integer.parseInt(inst.substring(6, 32),2)*4),28) + " and " + muxbranch.output + "\n";
        wireOut+="Jump Mux Output: " + muxj.output + "\n";
        wireOut+="JR Mux Input: " + muxMem.output + " and " + muxj.output + "\n";
        wireOut+="JR Mux Output: " + muxjr.output + "\n";
        wireOut+="Register Destination signal: " + ControlUnit.isRegDst() + "\n";
        wireOut+="Jump signal: "+ControlUnit.isJump()+"\n";
        wireOut+="Branch signal: " + ControlUnit.isBranch() + "\n";
        wireOut+="Memory Read signal: "+ ControlUnit.getMemRead()+ "\n";
        wireOut+="Memory to Register signal: " + ControlUnit.isMemtoReg() + "\n";
        wireOut+="ALUop signal: " + ControlUnit.getALUOPs() + "\n";
        wireOut+="Memory Write signal: " + ControlUnit.getMemWrite() + "\n";
        wireOut+="ALUsrc signal: " + ControlUnit.isAlusrc() + "\n";
        wireOut+="Register Write signal: " + ControlUnit.isRegWrite() + "\n";
        wireOut+="Link signal: " + ControlUnit.isLink() + "\n";
        wireOut+="JR signal: " + ControlUnit.isJr() + "\n";
        wireOut+="JALOp signal: " + ControlUnit.isJalop() + "\n";
        return wireOut;
    }
    public static String intToBinary(int n,int numofBits)
    {
        String Binary = "";
        for(int i=0;i<numofBits;++i,n/=2)
        {
            switch(n%2)
            {
                case 0:
                   Binary="0"+Binary ;
                   break;
                case 1:
                    Binary="1"+Binary;
                
            }
        }
        return Binary;
    }
}
   

