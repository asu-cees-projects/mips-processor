package main_project;


import java.util.Scanner;

public class insMem {
     static Instruction[] ins;   //array to keep 32 bit instructions as instructions
     static int pc;   //Program counter / base address for the instruction memory
     static int currentPC; // address to be received from the mux in the diagram
     static int i; //index of the int array
     static Instruction insOutput;//instruction to be sent out by memory
     static int clk; //Clock cycle counter
    public insMem() //constructor
    {
        
    }
    public static void start(int p,int n)
    {
        pc=p;
        ins = new Instruction[n]; //Under assumption that the user won't exceed 50 instructions
        i=0; //index starts at 0
        currentPC=pc;// counter starts at base address previously entered by user

    }
    public static void insert() // function to insert a new instruction into the array
    {
      /*  for(int j=0;j<ins.length;j++,i++)
        {//inputs for the GUI to determine instruction
        int type; //whether R(0), I(1) or J(2) type
        int opcode;
        int rs;
        int rt;
        int rd;
        int fn;
        int shamt;
        int constant;
        int jaddress;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter instruction type (0 for R, 1 for I, 2 for J):");
        type = sc.nextInt(); //to input instruction type as an integer
        if(type==0)//R type
        {
            //ins[i]=new R_type(); //object is created as an R type instruction
            opcode=0;
            System.out.println("Enter Function code:");
            fn=sc.nextInt();
            System.out.println("Enter rs register:");
            rs=sc.nextInt();
            if(fn==8) //jr function code is 8 and it only uses rs in the instruction
            {
                rt=0;
                rd=0;
            }
            else
            {
            System.out.println("Enter rt register:");
                rt=sc.nextInt();
            System.out.println("Enter rd register:");
                rd=sc.nextInt();
            }
            if(fn==0) //Function code of sll is 0
            {
            System.out.println("Enter shift amount:");
                shamt=sc.nextInt(); //is sll is selected the user will enter a shift amount
            }
            else
            {
                shamt=0; //otherwise shift amount is 0
            }
            ins[i]=new R_type(opcode,rs,rt,rd,shamt,fn);
        }
        else if(type==1)// I type
        {
            //ins[i]=new iType(); //object is created as an I type instruction
            System.out.println("Enter opcode:");
            opcode=sc.nextInt();
            System.out.println("Enter rs register:");
            rs=sc.nextInt();
            System.out.println("Enter rt register:");
            rt=sc.nextInt();
            System.out.println("Enter constant:");
            constant=sc.nextInt();
            ins[i]=new I_type(opcode,rs,rt,constant);
        }
        else//J type
        {
            //ins[i]=new jType(); //object is created as a J type instruction
            System.out.println("Enter opcode:");
            opcode=sc.nextInt();
            System.out.println("Enter target address:");
            jaddress=sc.nextInt();// User enters index of instruction to be jumped to
            ins[i]=new J_type(opcode,jaddress);
        }}*/
      //Habd 17
        int numofIns=Integer.parseInt(MIPS_Simulator.instructionsNum.getText());
        int p=Integer.parseInt(MIPS_Simulator.insAddress.getText());
        pc=p;
        currentPC=pc;
        i=0;
        ins=new Instruction[numofIns];
        String[] allIns=MIPS_Simulator.inputField.getText().split("\n");
        for(int j=0;j<numofIns;j++)
        {
            
        String f1="" ,f2="" ,f3="",cons,name;
        //String labels[];
        //int label;
    //private static enum type {r,j,i};


    //declare name and fields as not to be inside function only

    //methods

    //instruction decoding
   
        //name finder
        if(!(allIns[j].length()<4))
        {name = allIns[j].substring(0,4);}
        else
        {
            name = allIns[j].substring(0,3);
        }
        char[] x = name.substring(0,3).toCharArray();
        if (x[0]=='j' && x[1]=='r')
        {
            name ="jr";
        }
        else if(x[0]=='j' && x[1]=='a')
        {
            name="jal";
        }
        else if(x[0]=='j' && x[1]==' ')
        {
            name = "j";
        }
        else if (x[0]=='s' && x[1]=='w')
        {
            name = "sw";
        }
        else if (x[0]=='l' && x[1]=='w')
        {
            name = "lw";
        }
        else if (x[0]=='l' && x[1]=='b' && x[2]!='u')
        {
            name = "lb";
        }
        else if (x[0]=='s' && x[1]=='b')
        {
            name = "sb";
        }
        else if(x[0]=='b' && x[1]=='e')
        {
            name = "beq";
        }
        else
        {
            //add labels condition  ( label1 : )
            name =name.trim();
        }

        System.out.println("name : " + name);

        int ioff$=0 ; // index of first "$"

        //first field finder

        if (!(name.equals("j")) && !(name.equals("jal")) && !(name.equals("beq")))
        {
            if (name == "jr")
            {
                ioff$ = allIns[j].indexOf("$");
                f1 = allIns[j].substring(ioff$+1,ioff$+3).trim();
                System.out.println("first source : "+f1);
                ins[j] = new R_type(0,31,0,0,0,8);
                i++;
                continue;
            }
            else
            {
                ioff$ = allIns[j].indexOf("$");
                f1 = allIns[j].substring(ioff$,ioff$+3).trim();
                System.out.println("first field : "+f1);
            }

        }

        else
        {
            System.out.println("nothing");
            //labels for jump and jump and link
            //return from function after this else executes
            //return;
             if(name =="j")
             {
                 int l=allIns[j].indexOf(" ");
                 int jadd=Integer.parseInt(allIns[j].substring(l).trim());
                 ins[i]=new J_type(2,jadd);
                 i++;
                 continue;
             }
             else if(name=="jal")
             {
                 int l=allIns[j].indexOf(" ");
                 int jadd=Integer.parseInt(allIns[j].substring(l).trim());
                 ins[i]=new J_type(3,jadd);
                 i++;
                 continue;
             }
             else if(name=="beq")
             {
                 f1=allIns[j].substring(4,7).trim();
                 f2=allIns[j].substring(8,11).trim();
                 cons=allIns[j].substring(12).trim();
                 ins[i]=new I_type(4, regrt(f1), regrt(f2), negDeal(cons));
                 i++;
                 continue;
             }
        }

        //second field finder

        int iofs$=0 , iofb=0;       //index of second "$" // index of comma

        //case of normal second fields

        if (!(name.equals("lb")) && !(name.equals("lbu")) && !(name.equals("sb")) && !(name.equals("lw")) && !(name.equals("sw")) && !(name.equals("j")) && !(name.equals("jal")))
        {
            iofs$=allIns[j].indexOf("$" , ioff$+1);
            f2 = allIns[j].substring(iofs$,iofs$+3);
            System.out.println("second field : "+f2);
        }
        // case of j and jal instructions

        //case of load and store instruction
        else
        {
            iofb =  allIns[j].indexOf(",");
            int iofb2 = allIns[j].indexOf("(");
            cons = allIns[j].substring(iofb+1,iofb2);
            iofs$=allIns[j].indexOf("$" , ioff$+1);
            f2 = allIns[j].substring(iofs$,iofs$+3);
            System.out.println("second field : "+f2);
            System.out.println("constant = " + cons);

            //creating instruction
            if (name.equals("lb"))
            {
                ins[i] = new I_type(32,regrt(f2),regrt(f1),negDeal(cons));
                i++;
                continue;
            }
            else if (name.equals("lbu"))
            {
                ins[i] = new I_type(36,regrt(f2),regrt(f1),negDeal(cons));
                i++;
                continue;
            }
            else if (name.equals("sb"))
            {
                ins[i] = new I_type(40,regrt(f2),regrt(f1),negDeal(cons));
                i++;
                continue;
            }
            else if (name.equals("lw"))
            {
                ins[i] = new I_type(35,regrt(f2),regrt(f1),negDeal(cons));
                i++;
                continue;
            }
            else if (name.equals("sw"))
            {
                ins[i] = new I_type(43,regrt(f2),regrt(f1),negDeal(cons));
                i++;
                continue;
            }


        }


        //third field finder

        int ioft$=0,iofc=0; //index of third $ // [index of comma

        //case of 3 register fileds (add , nor , slt )

        if (!(name.equals("addi")) && !(name.equals("slti")) && !(name.equals("sll")))
        {
            ioft$ = allIns[j].indexOf("$" , iofs$+1);
            f3 = allIns[j].substring(ioft$,allIns[j].length());
            System.out.println("third field : "+f3);

            //creating instruction
            if (name.equals("add"))
            {
                ins[i] = new R_type(0,regrt(f2),regrt(f3),regrt(f1),0,32);
                i++;
                continue;
            }
            else if (name.equals("nor"))
            {
                ins[i] = new R_type(0,regrt(f2),regrt(f3),regrt(f1),0,39);
                i++;
                continue;
            }
            else if (name.equals("slt"))
            {
                ins[i] = new R_type(0,regrt(f2),regrt(f3),regrt(f1),0,42);
                i++;
                continue;
            }
        }
        //add case of beq instruction

        //case of addi , slti ,and sll
        else
        {
            iofc=allIns[j].indexOf("," , iofs$+1);
            cons = allIns[j].substring(iofc+1);
            System.out.println("constant = " + cons);

            if(name.equals("sll"))
            {
                ins[i] = new R_type(0,0,regrt(f2),regrt(f1),Integer.parseInt(cons),0);
                i++;
                continue;
            }
            else if (name.equals("addi"))
            {
                ins[i] = new I_type(8,regrt(f2),regrt(f1),negDeal(cons));
                i++;
                continue;
            }
            else if (name.equals("slti"))
            {
                ins[i] = new I_type(10,regrt(f2),regrt(f1),negDeal(cons));
                i++;
                continue;
            }

        }

    }
    }
    /*public static void create_ins(String n)
    {
        if (n.equals("add"))
    }*/
    public static int regrt(String s)
    {
        if(s.equals("$0 ")||s.equals("$zero") ||s.equals("$0")||s.equals("$0,")||s.equals("$ze"))
        {
            return 0 ;
        }
        else if (s.equals("$at"))
        {
            return 1;
        }
        else if (s.equals("$v0"))
        {
            return 2;

        }
        else if (s.equals("$v1"))
        {
            return 3;
        }
        else if (s.equals("$a0"))
        {
            return 4;

        }
        else if (s.equals("$a1"))
        {
            return 5;
        }
        else if (s.equals("$a2"))
        {
            return 6;
        }
        else if (s.equals("$a3"))
        {
            return 7;
        }
        else if (s.equals("$t0"))
        {
            return 8;
        }
        else if (s.equals("$t1"))
        {
            return 9;
        }
        else if (s.equals("$t2"))
        {
            return 10;
        }
        else if (s.equals("$t3"))
        {
            return 11;
        }
        else if (s.equals("$t4"))
        {
            return 12;
        }
        else if (s.equals("$t5"))
        {
            return 13;
        }
        else if (s.equals("$t6"))
        {
            return 14;
        }
        else if (s.equals("$t7"))
        {
            return 15;
        }
        else if (s.equals("$s0"))
        {
            return 16;
        }
        else if (s.equals("$s1"))
        {
            return 17;
        }
        else if (s.equals("$s2"))
        {
            return 18;
        }
        else if (s.equals("$s3"))
        {
            return 19;
        }
        else if (s.equals("$s4"))
        {
            return 20;
        }
        else if (s.equals("$s5"))
        {
            return 21;
        }
        else if (s.equals("$s6"))
        {
            return 22;
        }
        else if (s.equals("$s7"))
        {
            return 23;
        }
        else if (s.equals("$t8"))
        {
            return 24;
        }
        else if (s.equals("$t9"))
        {
            return 25;
        }
        else if (s.equals("$k0"))
        {
            return 26;
        }
        else if (s.equals("$k1"))
        {
            return 27;
        }
        else if (s.equals("$gp"))
        {
            return 28;
        }
        else if (s.equals("$sp"))
        {
            return 29;
        }
        else if (s.equals("$fp"))
        {
            return 30;
        }
        else if (s.equals("$ra"))
        {
            return 31;
        }
        else
        {
            return 1;
        }
    

    }
    public static void execute()//function to carry out stored instructions
    {
        if(currentPC<(pc+(4*i)))//pc+4i is the address directly after the last instruction entered by the user into the memory so when it is reached execution is terminated
        {
            insOutput=ins[(currentPC-pc)/4]; // equation to calculate index of required instruction in the array
            
        }
        else
        {
            end();
        }
    }
    public static Instruction getInsOutput()
    {
        return insOutput;
    }
    public static int getCurrentPC()
    {
        return currentPC;
    }
    public static void setCurrentPC(int p)
    {
        currentPC=p;// new value of current PC to be received from the mux in the diagram
        //execute();
    }
    public static void end()
    {
        System.out.println("No more instructions to be executed");
        //MIPS_Simulator.nextBtn.setEnabled(false);
    }
    public static int negDeal(String s)
    {
        char[] chara=s.toCharArray();
        if(chara[0]=='-')
        {
            return -1*Integer.parseInt(s.substring(1));
        }
        else
            return Integer.parseInt(s);
    }
}