package main_project;


public class ALUcontrol
{
    //fields
    private static int ALUOP;           // refers to the alu operation code signal received from control unit
     static int funct;           // refers to the function code of the instruction (ignored in case of i-type instructions)
    private static int alucontrol;      // refers to the output signal sent to the ALU to select which operation to carry

    //constructor
    public static void use(int aluOp, int fn)
    {
        ALUOP = aluOp;
        funct = fn;
        if (ALUOP == 0 )                 //in case of lw and sw instruction we need to compute required memory address by addition
        {
            alucontrol=0b0010;
            System.out.println("add needed by alu");
        }
        else if (ALUOP == 0b01)          //in case of branch instructions we need to subtract to check equality
        {
            alucontrol = 0b0110;
            System.out.println("subtract needed by alu");
        }
        else if (ALUOP == 0b10)         //in case of r-type instructions the operation chosen to be carried is specified by the function code
        {                                    //in addition to the ALU operation code
            switch (funct)
            {
                case 0b100000:                   //"0b" refers that the following number is in binary (kaselt a7awelhom decimal fa copy paste)
                    alucontrol=0b0010;      // add instruction requires addition in alu
                    System.out.println("add instruction");
                    break;
                case 0b100010:
                    alucontrol=0b0110;     // subtract instruction requires subtraction in alu
                    System.out.println("subtract instruction");
                    break;
                case 0b100100:
                    alucontrol=0b0000;     // for bitwise anding
                    System.out.println("and instruction");
                    break;
                case 0b100101:
                    alucontrol=0b0001;     // for bitwise oring
                    System.out.println("or instruction");
                    break;
                case 0b101010:
                    alucontrol=0b0111;    // for set less than
                    System.out.println("slt instruction");
                    break;
                case 0b100111:
                    alucontrol=0b0011;    // for bitwise noring
                    System.out.println("nor instruction");
                    break;
                case 0 :
                    alucontrol=0b1000;
                    System.out.println("sll instruction");
                    break;
                case 8 :                                       //alu will not be used any way so the
                                                               //aluop and alusrc signals from the control unit are dont care values
                    alucontrol=0b0010;
                    System.out.println("jr instruction");
                    muxjr.signal=true;
                    break;
            }
            
        }
        else if (ALUOP==3)
                    {
                    alucontrol = 7;
                    
                    }
    }

    //getters
    public static int getAlucontrol()
    {
        return alucontrol;
    }
}
