
package main_project;
import java.util.*;


class dataandAddress
{
    public int Address;
    public int dataInt;
    public String dataBin;
    public dataandAddress(int a, int di,String db)
    {
        this.Address=a;
        this.dataInt=di;
        this.dataBin=db;
    }
}
public class dataMem {
    public static ArrayList<dataandAddress> mem=new ArrayList<dataandAddress>();
    public static int inAddress;
    public static int wData;
    public static int readData;
    public static int memWrite;
    public static int memRead;
    public static int reMemEmu;
    public static void test(int a,int b, int c, int d, int e)
    {
        inAddress=a;
        wData=b;
        memWrite=c;
        memRead=d;
        reMemEmu=e;
    }
    public static void fillMem()
    {
        int Add= Integer.parseInt(MIPS_Simulator.toMemAdd.getText());
        int Val= Integer.parseInt(MIPS_Simulator.toMemVal.getText());
        if(isFound(Add))
        {
            find(Add).dataInt=Val;
            find(Add).dataBin=intToBinary(Val, 32);
        }
        else
        {
            mem.add(new dataandAddress(Add, Val, intToBinary(Val, 32)));
        }
    }
    public static void disPlayMem()
    {
        System.out.println("Address  :  Data  :  Binary");
        MIPS_Simulator.DMTF.setText("Address:Data\n");
        for(int i=0;i<mem.size();i++)
        {
            System.out.println(mem.get(i).Address+"  :  "+mem.get(i).dataInt+"  :  "+mem.get(i).dataBin);
            MIPS_Simulator.DMTF.setText(MIPS_Simulator.DMTF.getText()+mem.get(i).Address+":"+mem.get(i).dataInt+"\n");
        }
    }
    public static dataandAddress find(int addr)
    {
        for(int i=0;i<mem.size();i++)
        {
            if(mem.get(i).Address==addr)
            {
                return mem.get(i);
            }
        }
        return new dataandAddress(addr, 0, intToBinary(0, 32));
    }
    public static boolean isFound(int addr)
    {
        for(int i=0;i<mem.size();i++)
        {
            if(mem.get(i).Address==addr)
            {
                return true;
            }
        }
        return false;
    }
    public static void start()
    {
        inAddress=ALU.getOutput();
        wData=RF.getReadD2();
        memWrite=ControlUnit.getMemWrite();
        memRead=ControlUnit.getMemRead();
        if(memWrite==1 && memRead==0)
        {
            dataandAddress toAdd=new dataandAddress(inAddress, wData, intToBinary(wData, 32));
            if(isFound(inAddress))
            {
                find(inAddress).dataInt=wData;
                find(inAddress).dataBin=intToBinary(wData, 32);
            }
            else
                mem.add(toAdd);
            readData=0;
        }
        else if(memWrite==0&& memRead==1)
        {
            if(isFound(inAddress))
            {
                readData=find(inAddress).dataInt;
            }
            else
            {
                readData=0;
            }
        }
        else if(memWrite ==0 && memRead ==2)
        {
            if(isFound(inAddress)||isFound(inAddress-1)||isFound(inAddress-2)||isFound(inAddress-3))
            {
                if((inAddress-1)==RF.getReadD1() ||( inAddress==RF.getReadD1()&& inAddress%4==1))
                    {
                        if(isNegative(signExtend(find(inAddress-1).dataBin.substring(8,16))))
                            readData=-1*Integer.parseInt(twosComp(signExtend(find(inAddress-1).dataBin.substring(8,16))),2);
                        else
                            readData=Integer.parseInt(signExtend(find(inAddress-1).dataBin.substring(8,16)),2);
                        
                    }
                else if((inAddress-2)==RF.getReadD1()||( inAddress==RF.getReadD1()&& inAddress%4==2))
                {
                        if(isNegative(signExtend(find(inAddress-2).dataBin.substring(16,24))))
                            readData=-1*Integer.parseInt(twosComp(signExtend(find(inAddress-2).dataBin.substring(16,24))),2);
                        else
                             readData=Integer.parseInt(signExtend(find(inAddress-2).dataBin.substring(16,24)),2);
                }
                else if((inAddress-3)==RF.getReadD1()||( inAddress==RF.getReadD1()&& inAddress%4==3))
                {
                        if(isNegative(signExtend(find(inAddress-3).dataBin.substring(24,32))))
                            readData=-1*Integer.parseInt(twosComp(signExtend(find(inAddress-3).dataBin.substring(24,32))),2);
                        else
                            readData=Integer.parseInt(signExtend(find(inAddress-3).dataBin.substring(24,32)),2);
                }
                else
                {
                        if(isNegative(signExtend(find(inAddress).dataBin.substring(0,8))))
                            readData=-1*Integer.parseInt(twosComp(signExtend(find(inAddress).dataBin.substring(0,8))),2);
                        else
                            readData=Integer.parseInt(signExtend(find(inAddress).dataBin.substring(0,8)),2);
                }
            }
        }
        else if (memWrite == 2 && memRead == 0)
        {
            if(isFound(inAddress)||isFound(inAddress-1)||isFound(inAddress-2)||isFound(inAddress-3))
            {
                if((inAddress-1)==RF.getReadD1() ||( inAddress==RF.getReadD1()&& inAddress%4==1))
                    {
                        String s1 = find(inAddress-1).dataBin.substring(0,8);
                        String s2 = find(inAddress-1).dataBin.substring(16,32);
                        String resBin = s1+intToBinary(wData, 8)+s2;
                        int resInt = Integer.parseInt(resBin,2);
                        find(inAddress-1).dataBin=resBin;
                        find(inAddress-1).dataInt=resInt;                        
                    }
                else if((inAddress-2)==RF.getReadD1()||( inAddress==RF.getReadD1()&& inAddress%4==2))
                {
                        String s1 = find(inAddress-2).dataBin.substring(0,16);
                        String s2 = find(inAddress-2).dataBin.substring(24,32);
                        String resBin = s1+intToBinary(wData, 8)+s2;
                        int resInt = Integer.parseInt(resBin,2);
                        find(inAddress-2).dataBin=resBin;
                        find(inAddress-2).dataInt=resInt;     
                }
                else if((inAddress-3)==RF.getReadD1()||( inAddress==RF.getReadD1()&& inAddress%4==3))
                {
                        String s1 = find(inAddress-3).dataBin.substring(0,24);
                        String resBin = s1+intToBinary(wData, 8);
                        int resInt = Integer.parseInt(resBin,2);
                        find(inAddress-3).dataBin=resBin;
                        find(inAddress-3).dataInt=resInt;     
                }
                else
                {
                        String s2 = find(inAddress).dataBin.substring(8,32);
                        String resBin = intToBinary(wData, 8)+s2;
                        int resInt = Integer.parseInt(resBin,2);
                        find(inAddress).dataBin=resBin;
                        find(inAddress).dataInt=resInt;     
                }
            }
            else 
            {
                if(inAddress%4==0)
                {
                    dataandAddress toAdd=new dataandAddress(inAddress, (int)Math.pow(2,24)*wData, intToBinary((int)Math.pow(2,24)*wData, 32));
                    mem.add(toAdd);
                }
                else if(inAddress%4==1)
                {
                    dataandAddress toAdd=new dataandAddress(inAddress-1, (int)Math.pow(2,16)*wData, signExtendlb(intToBinary((int)Math.pow(2,16)*wData, 24),1));
                    mem.add(toAdd);
                }
                else if(inAddress%4==2)
                {
                    dataandAddress toAdd= new dataandAddress(inAddress-2, (int)Math.pow(2,8)*wData, signExtendlb(intToBinary((int)Math.pow(2,8)*wData, 16),2));
                    mem.add(toAdd);
                }
                else
                {
                    dataandAddress toAdd = new dataandAddress(inAddress-3, wData, signExtendlb(intToBinary(wData, 8),3));
                    mem.add(toAdd);
                }
            }
            
        }
        else if (memWrite==0 && memRead==3)
            {
                if(isFound(inAddress)||isFound(inAddress-1)||isFound(inAddress-2)||isFound(inAddress-3))
            {
                if((inAddress-1)==RF.getReadD1())
                    {
                        readData=Integer.parseInt("00000000"+(find(inAddress-1).dataBin.substring(8,16)+"0000000000000000"),2);
                    }
                else if((inAddress-2)==RF.getReadD1())
                {
                    readData=Integer.parseInt("0000000000000000"+(find(inAddress-2).dataBin.substring(16,24))+"00000000",2);
                }
                else if((inAddress-3)==RF.getReadD1())
                {
                    readData=Integer.parseInt("000000000000000000000000"+(find(inAddress-3).dataBin.substring(24,32)),2);
                }
                else
                {
                    readData=Integer.parseInt((find(inAddress).dataBin.substring(0,8))+"000000000000000000000000",2);
                }
            }
            }
    }
    public static String intToBinary (int n, int numOfBits)//static function to convert integer values to a binary String with a specific number of bits
    {
        String binary = "";
        for(int i = 0; i < numOfBits; ++i, n/=2) 
        {
            switch (n % 2) 
            {
                case 0:
                binary = "0" + binary;
                break;
                case 1:
                binary = "1" + binary;
            }
        }
        return binary;
    }
    public static String signExtend(String s)
    {
        char[] chara= s.toCharArray();
        if(chara[0]=='1')
        {
            s="111111111111111111111111"+s;
        }
        else
        {
            s="000000000000000000000000"+s;
        }
        return s;
    }
    public static String signExtendlb(String s, int i)
    {
        char[] chara= s.toCharArray();
        
        for(int j=0;j<8*i;j++)
        {
            if(chara[0]=='0')
            {
                s="0"+s;
            }
            else
            {
                s="1"+s;
            }
        }
        return s;
    }
    public static boolean isNegative (String s)
    { 
        char [] x = s.toCharArray();
        if (x[0] == '1')
            return true ;
        return false ;
    }
    public static String twosComp(String s)
    {
        char [] chara=s.toCharArray();
        String res="";
        boolean firstFlag=false;
        int firstInd=0;
        for(int i=chara.length-1;i>-1;i--)
        {
            if(firstFlag)
            {
                break;
            }
           else
            {
                if(chara[i]=='1')
                {   firstFlag=true;
                    firstInd=i;
                }
                    }
        }
        if(firstFlag)
        {
                for(int i=0;i<firstInd;i++)
        {
            res+=reverse(chara[i]);
        }
                for(int i=firstInd;i<chara.length;i++)
                {
                    res+=chara[i];
                }
                
        }
        return res;
    }
    public static char reverse(int i)
    {
        if(i=='1')
        {
            return '0';
        }
        else
            return '1';
    }
            }
