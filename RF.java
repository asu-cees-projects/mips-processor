package main_project;


public class RF {
    //fields
    static int []  Regs = new int[32];
    private static int readR1 ,readR2;               // register number to be read
    private static int readD1 , readD2;              //data to be written to number of register given
    private static int WR ;                          // number of register to be written in
    private static int WD ;                          // data to be written

    //methods

    //method to set register file
    public static void RFS()
    {
        readR1 = insMem.getInsOutput().rs;
        readR2 = insMem.getInsOutput().rt;
        readD1=Regs[readR1];
        readD2=Regs[readR2];
    }
    public static void RFSm ()
    {
        WR=muxlink.output;
        boolean x =ControlUnit.isRegWrite();
        if (x==true&&WR!=0)
        {
            WD = muxjalop.output;
            Regs[WR]=WD;
        }
    }

    public static int getReadD1()
    {
        return readD1;
    }

    public static int getReadD2()
    {
        return readD2;
    }
    public static void printRegs()
    {
        System.out.println("Register:\tValue");
        for(int i=0;i<32;i++)
        {System.out.println(i+"\t:\t"+Regs[i]);
            MIPS_Simulator.regs.setValueAt(""+Regs[i], i, 1);}
        
    }
    public static void adjustREG()
    {
        if(insMem.regrt(MIPS_Simulator.regNameIN.getText()) != 0){
        Regs[insMem.regrt(MIPS_Simulator.regNameIN.getText())]=Integer.parseInt(MIPS_Simulator.regValIN.getText());
        }
        printRegs();
    }
    public static void setSP()
    {
        Regs[29]=16000+Integer.parseInt(MIPS_Simulator.insAddress.getText());
                printRegs();
    }
}
