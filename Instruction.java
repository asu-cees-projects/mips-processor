package main_project;
    

    public abstract class Instruction
    {
        //fields
       // protected int[] insfields;             //refers to the array of integers that will contain instruction fields (opcode,rs,rt...etc)
         int opcode;
         int rs;
         int rt;
         int rd;
         int shamt;
         int fn;
         int offset;
         int jaddress;

        //constructor
        public Instruction(int opcode,int rs,int rt,int rd,int shamt,int fn,int offset,int jaddress)
        {
            //this.insfields = insfields;
            this.opcode = opcode;
            this.rs=rs;
            this.rt=rt;
            this.rd=rd;
            this.shamt=shamt;
            this.fn=fn;
            this.offset=offset;
            this.jaddress=jaddress;
        }

        //getters
        public  int getOpcode()
        {
            return opcode;
        }

    public  int getRs() {
        return rs;
    }

    public  int getRt() {
        return rt;
    }

    public  int getConstant() {
        return offset;
    }
 public  int getJaddress()
    {
        return jaddress;
    }

    public  int getRd() {
        return rd;
    }

    public  int getShamt() {
        return shamt;
    }

    public  int getFunct() {
        return fn;
    }   //method
        //public abstract void fillfields();    // methods that fills tha array with the corresponding int for each field
    }
